import asyncio
import functools
import signal

from aiohttp.web import Application as Server
from aiohttp.web import run_app
from application import Application

SETTINGS = {
    "host": "0.0.0.0",
    "port": 8080,
}


def start_server(host, port):
    try:
        run_server(host, port)
    except Exception as e:
        print(e)


def stop_server():
    asyncio.get_event_loop().stop()


def run_server(host, port, handle_signals=False):
    event_loop = asyncio.get_event_loop()
    event_loop.add_signal_handler(signal.SIGINT, functools.partial(stop_server))
    event_loop.add_signal_handler(signal.SIGTERM, functools.partial(stop_server))

    server = Server()
    init_handlers(server)
    run_app(server, host=host, port=port, handle_signals=handle_signals)


def init_handlers(server):

    server.router.add_get(
        "/get_all_companies_in_building",
        Application.get_all_companies_in_building
    )

    server.router.add_get(
        "/get_companies_with_rubric",
        Application.get_companies_with_rubric
    )

    server.router.add_get(
        "/get_companies_in_radius",
        Application.get_companies_in_radius
    )

    server.router.add_get(
        "/get_buildings_list",
        Application.get_buildings_list
    )

    server.router.add_get(
        "/find_companies",
        Application.find_companies
    )

    server.router.add_get(
        "/get_companies_with_subrubrics",
        Application.get_companies_with_subrubrics
    )

if __name__ == "__main__":
    start_server(
        **SETTINGS
    )
