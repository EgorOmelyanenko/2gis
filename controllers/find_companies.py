import re
from bson.errors import InvalidId
from bson.objectid import ObjectId

from controllers.common import full_information, get_company, return_data, return_error
from database.database import get_connection


def find_companies(request):
    db = get_connection()["2gis_db"]
    companies = [
        company for company in
        db["companies"].find(
            prepara_fields_for_search(request.rel_url.query)
        )
    ]

    return return_data(
        [
            company["name"] if not full_information(request) else get_company(db, company)
            for company in companies
        ]
    ) if companies else return_error("Компании не найдены")


def prepara_fields_for_search(dict_):
    fields_for_search = dict(dict_)

    if "id" in fields_for_search:

        try:
            ids = [ObjectId(id_) for id_ in fields_for_search["id"].split(",")]
        except InvalidId:
            return {}

        return {"_id": {
            "$in": ids
        }}

    if "name" in fields_for_search:
        return {
            "name": re.compile(".*{}.*".format(fields_for_search["name"]))
        }
