from controllers.common import get_company, return_data, return_error, full_information
from database.database import get_connection, get_where_field_in_array


def get_companies_with_subrubrics(request):
    db = get_connection()["2gis_db"]

    rubric = db["rubrics"].find_one({
        "rubric": request.rel_url.query["rubric"]
    })

    if not rubric:
        return return_error("Такой рубрики не существует")


    companies = get_where_field_in_array(
        db,
        "companies",
        "rubrics",
        _get_subrubrics(
            db,
            rubric
        )
    )

    return return_data(
        [
            company["name"] if not full_information(request) else get_company(db, company)
            for company in companies
        ]
    )


def _get_subrubrics(connection, rubric):
    ids = []
    ids.append(rubric["_id"])

    if "next_rubrics" in rubric:
        for subrubric in rubric["next_rubrics"]:
            for id_ in _get_subrubrics(
                connection,
                connection["rubrics"].find_one({
                    "_id": subrubric
                })
            ):
                ids.append(id_)
    return ids
