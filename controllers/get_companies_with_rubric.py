from controllers.common import get_company, return_data, return_error, full_information
from database.database import get_connection


def get_companies_with_rubric(request):
    db = get_connection()["2gis_db"]

    rubric = db["rubrics"].find_one({
        "rubric": request.rel_url.query["rubric"],
    })

    if not rubric:
        return return_error("Такой рубрики не существует")

    companies = []
    for company in db["companies"].find({
            "rubrics": rubric["_id"]
    }):
        companies.append(company)


    return return_error("Нет компаний с этой рубрикой") if not companies else return_data(
        [
            get_company(db, company) if full_information(request)
            else
            company["name"] for company in companies
        ]
    )
