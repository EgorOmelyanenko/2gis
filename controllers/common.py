import json

from aiohttp.web import Response

from database.database import get_where_field_in_array


def get_company_building(connection, company_id):
    building = connection["buildings"].find_one({"_id": company_id})
    return "{}, {}".format(
        building["street"],
        building["building"]
    )

def get_company_rubrics(connection, rubrics_ids):
    return [
        rubric["rubric"] for rubric in
        get_where_field_in_array(
            connection,
            "rubrics",
            "_id",
            rubrics_ids
        )
    ]

def get_company(connection, company):
    return {
        "id": str(company["_id"]),
        "name": company["name"],
        "phones": company["phones"],
        "address": get_company_building(
            connection,
            company["building"]
        ),
        "rubrics": get_company_rubrics(
            connection,
            company["rubrics"]
        )
    }

def return_error(message):
    return Response(
        text=json.dumps(
            {
                "error": message,
            },
            ensure_ascii=False
        )
    )

def return_data(data):
    return Response(
        text=json.dumps(
            data,
            ensure_ascii=False
        ),
    )

def full_information(request):
    if "full_information" in request.rel_url.query:
        if str.lower(request.rel_url.query["full_information"]) == "true":
            return True
    return False
