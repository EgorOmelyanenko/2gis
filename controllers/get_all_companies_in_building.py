from database.database import get_connection, get_where_field_in_array
from controllers.common import return_data, return_error, full_information, get_company


def get_all_companies_in_building(request):

    db = get_connection()["2gis_db"]

    building = db["buildings"].find_one({
        "street": request.rel_url.query["street"],
        "building": request.rel_url.query["building"]
    })

    if not building:
        return return_error("Такого здания не существует")

    if "companies" not in building:
        return return_error("В этом здании нет компаний")

    return return_data([
        company["name"] if not full_information(request) else get_company(db, company)
        for company in get_where_field_in_array(
            db,
            "companies",
            "_id",
            building["companies"]
        )
    ])
