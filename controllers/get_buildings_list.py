from database.database import get_connection, get_where_field_in_array
from controllers.common import return_data, return_error, full_information

def get_buildings_list(request):

    db = get_connection()["2gis_db"]

    buiildings = db["buildings"].find({})

    if not buiildings:
        return return_error("Здания отсутствуют")

    return return_data([
        get_building(db, building, full_information(request)) for building in buiildings
    ])


def get_building(connection, building, full_information_):
    return {
        "address": "{}, {}".format(building["street"], building["building"]),
    } if not full_information_ else {
        "address": "{}, {}".format(building["street"], building["building"]),
        "companies": [
            company["name"] for company in
            get_where_field_in_array(
                connection,
                "companies",
                "_id",
                building["companies"]
            )
        ]
    }
