from geopy.distance import vincenty

from database.database import get_connection, get_where_field_in_array
from controllers.common import get_company, return_data, return_error, full_information

# неудачное решение, поскольку происходит подсчет расстояний для всех зданий
# до заданной точки, что при большом количестве зданий просто недопустимо. Это принято для
# повышения точности расчетов, ибо второе решение (ниже в комментарии) дает гораздо меньшую точность
# но работает значительно быстрее. В планах вернуться к закомментированному решению, с целью
# увеличения точности, для чего требуются бОльшие знания в предметной области.

    # другая версия, которая вернет здания в прямоугольной области от заданной точки
    # dist = 20 # дистанция
    # mylon = 54.979622
    # mylat = 82.898479
    # lon1 = mylon - dist/abs(math.cos(math.radians(mylat))*111.111) # 1 градус широты ~ 111 км
    # lon2 = mylon + dist/abs(math.cos(math.radians(mylat))*111.111)
    # lat1 = mylat - (dist/111.111)
    # lat2 = mylat + (dist / 111.111)

    # db["buildings"].find({
    #         "longitude": {
    #             "$gt": lon1,
    #             "$lte": lon2
    #         },
    #         "latitude": {
    #             "$gt": lat1,
    #             "$lte": lat2
    #         }
    #     })

def get_companies_in_radius(request):
    db = get_connection()["2gis_db"]

    try:
        radius = float(request.rel_url.query["radius"])
        longtitude = float(request.rel_url.query["longtitude"])
        latitude = float(request.rel_url.query["latitude"])
    except ValueError:
        return return_error("Некорректные данные")

    start_point = (
        longtitude,
        latitude
    )

    companies_ids = []

    for building in db["buildings"].find({}):
        if vincenty(start_point, (building["longitude"], building["latitude"])).km <= radius:
            companies_ids.extend(building["companies"])

    if not companies_ids:
        return return_error("Компании не найдены")

    return return_data(
        [
            company["name"] if not full_information(request) else get_company(db, company) for
            company in get_where_field_in_array(
                db,
                "companies",
                "_id",
                companies_ids)
        ]
    )
