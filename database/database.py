import json

import pymongo


def get_connection():
    with open("database/settings.json", "r") as f:
        return pymongo.MongoClient(
            **json.loads(
                f.read()
            )
        )

def get_where_field_in_array(connection, document, field, array):
    return connection[document].find({
        field: {
            "$in": array
        }
    })

