import json
import random

from database import get_connection


def insert_buldings():
    with open("database/content/buildings.json", "r") as f:
        for building in json.loads(f.read()):
            db["buildings"].insert(building)


def insert_rubrics():
    with open("database/content/rubrics.json", "r") as f:
        for rubric in json.loads(f.read()):
            _insert_one_rubric(rubric)


def _insert_one_rubric(rubric):
    if "next_rubrics" in rubric:
        ids = []
        for _rubric in rubric["next_rubrics"]:
            ids.append(
                _insert_one_rubric(_rubric)
            )

        return db["rubrics"].insert({
            "rubric": rubric["rubric"],
            "next_rubrics": ids
        })

    return db["rubrics"].insert(rubric)


def insert_companies():
    with open("database/content/companies.json", "r") as f:
        names = _generate_companies_names(
            json.loads(f.read())
        )

    for name in names:
        company = {
            "name": name,
            "building": _get_random_ids("buildings", 1)[0],
            "rubrics": _get_random_ids("rubrics", random.randint(1, 3)),
            "phones": [random.randint(1000000, 9999999) for _ in range(1, random.randint(2, 5))]
        }

        _update_companies_in_building(
            company["building"],
            db["companies"].insert(company)
        )

# private

def _generate_companies_names(content, companies_count=100):
    names = []
    while len(names) < companies_count:
        name = "{} '{} {}'".format(
            random.choice(content["prefix"]),
            random.choice(content["first_part"]),
            random.choice(content["second_part"]),
        )
        if name not in names:
            names.append(name)
            yield name


def _get_random_ids(document, count):
    cursor = db[document].aggregate([{
        "$sample": {
            "size": count
        }
    }])

    return [x["_id"] for x in cursor]

def _update_companies_in_building(building_id, company_id):
    building = db["buildings"].find_one({
        "_id": building_id
    })

    companies_in_building = building["companies"] if "companies" in building else []
    companies_in_building.append(company_id)

    db["buildings"].update_one(
        {
            "_id": building_id
        },
        {
            '$set':
            {
                'companies': companies_in_building
            }
        }
    )


if __name__ == "__main__":
    db = get_connection()["2gis_db"]
    insert_buldings()
    insert_rubrics()
    insert_companies()
