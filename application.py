from controllers.common import return_error
from controllers.find_companies import find_companies
from controllers.get_all_companies_in_building import get_all_companies_in_building
from controllers.get_buildings_list import get_buildings_list
from controllers.get_companies_in_radius import get_companies_in_radius
from controllers.get_companies_with_rubric import get_companies_with_rubric
from controllers.get_companies_with_subrubrics import get_companies_with_subrubrics


# TODO: сделать методы асинхронными
# TODO: проверка типов переменных в словаре
class Application:

    @staticmethod
    def get_all_companies_in_building(request):
        if Application._check_required_keys(["building", "street"], request):
            return get_all_companies_in_building(request)

        return return_error("Укажите улицу и здание")

    @staticmethod
    def get_companies_with_rubric(request):
        if Application._check_required_keys(["rubric"], request):
            return get_companies_with_rubric(request)

        return return_error("Укажите рубрику")

    @staticmethod
    def get_companies_in_radius(request):
        if Application._check_required_keys(["radius", "longtitude", "latitude"], request):
            return get_companies_in_radius(request)

        return return_error("Укажите радиус (в километрах), долготу и широту")

    @staticmethod
    def get_buildings_list(request):
        return get_buildings_list(request)

    @staticmethod
    def find_companies(request):
        return find_companies(request)

    @staticmethod
    def get_companies_with_subrubrics(request):
        if Application._check_required_keys(["rubric"], request):
            return get_companies_with_subrubrics(request)

        return return_error("Укажите рубрику")

    @staticmethod
    def _check_required_keys(keys, request):
        for key in keys:
            if key not in request.rel_url.query:
                return False
        return True
